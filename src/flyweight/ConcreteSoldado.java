package flyweight;

public class ConcreteSoldado implements Soldado {
    // Estado intrínseco
    private Object representaoGrafica;

    @Override
    public void moverSoldado(int previousLocationX,
                            int previousLocationY,
                            int newLocationX,
                            int newLocationY) {
        // deletar representação do soldado da localização anterior
        // então renderizar a representação do soldado na nova localização

        System.out.println("Soldado movido de (x: " + previousLocationX + ", y: " + previousLocationY + ") para (x: " + newLocationX + ", y: " + newLocationY + ")");
    }
}
