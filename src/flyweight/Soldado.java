package flyweight;

public interface Soldado {
    /**
     * Move soldado da localização anterior para a nova localização
     * Note que a localização é extrínseca em relação a implementação do soldado
     */
    public void moverSoldado(int previousLocationX,
                             int previousLocationY,
                             int newLocationX,
                             int newLocationY);
}
