package flyweight;

/**
 * Flyweight Factory
 */

public class SoldierFactory {
    private SoldierFactory() {}

    /**
     * Pool for one soldier only
     * if there are more soldier types
     * this can be an array or list or better a HashMap
     *
     */
    private static Soldier SOLDIER;

    /**
     * getFlyweight
     * @return
     */
    public static synchronized Soldier getSoldier() {
        // this is a Singleton

        // if there is no soldier
        if (SOLDIER == null) {
            // create the soldier
            SOLDIER = new ConcreteSoldier();
        }

        // return the only soldier reference
        return SOLDIER;
    }
}
