package flyweight;

public class SoldadoFactory {
    private SoldadoFactory() {}

    /**
     * se houver mais tipos de soldados
     * esse tipo pode ser um array ou lista
     */
    private static Soldado SOLDADO;

    public static synchronized Soldado getSoldado() {
        // Esse é um Singleton

        // Se já não existe o objeto soldado
        if (SOLDADO == null) {
            // cria o soldado
            SOLDADO = new ConcreteSoldado();
        }

        return SOLDADO;
    }
}
