package flyweight;

public class Game {
    public static void main(String[] args) {
        // iniciar jogo
        // desenhar terreno

        // cria 2 soldados:
        SoldadoClient[] soldados = {
            new SoldadoClient(),
            new SoldadoClient()
        };

        // move cada soldado para uma localização
        soldados[0].moverSoldado(17, 2112);
        soldados[1].moverSoldado(137, 112);
    }
}
