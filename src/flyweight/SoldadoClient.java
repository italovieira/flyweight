package flyweight;

/**
 * esse é o cliente do flyweight soldado
 * este objeto é responsável por manter o estado que é extrínseco ao flyweight
 */
public class SoldadoClient {
    // Referência ao olyweight
    private Soldado soldado = SoldadoFactory.getSoldado();

    // Estados mantidos pelo cliente
    private int currentLocationX = 0;
    private int currentLocationY = 0;

    public void moverSoldado(int newLocationX, int newLocationY) {
        soldado.moverSoldado(currentLocationX,
                            currentLocationY,
                            newLocationX,
                            newLocationY);

        currentLocationX = newLocationX;
        currentLocationY = newLocationY;
    }
}
